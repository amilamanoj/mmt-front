'use strict';

// Register `useCaseAdd` component, along with its associated controller and template
angular.module('useCase-add').component('useCaseAdd', {
    templateUrl: 'usecase-add/usecaseadd.template.html',
    controller: ['UseCase', '$scope', '$mdToast',
        function UseCaseAddController(UseCase, $scope, $mdToast) {
            $scope.saveProject = function saveProject() {
                console.log("create usecase");
                $scope.newUseCase = new UseCase();
                $scope.newUseCase.title = $scope.usecase.title;
                $scope.newUseCase.body = $scope.usecase.body;

                console.log($scope.newUseCase);
                console.log($scope.usecase.title);

                $scope.newUseCase.$save()
                    .then(function () {
                        // $state.go('usecases');
                        showSimpleToast("Project created!")
                    }).catch(function (e) {
                    console.log("error: " + e);
                    showSimpleToast("Project creation failed: " + e);

                });
            };

            function showSimpleToast(txt) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(txt)
                        .position('bottom right')
                        .hideDelay(3000)
                );
            }
        }
    ]
});
