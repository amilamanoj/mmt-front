'use strict';

angular.module('myMeanApp').config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.when('/usecases', {
            template: '<use-case-list></use-case-list>'
        }).when('/addUsecase', {
            template: '<use-case-add></use-case-add>'
        }).when('/usecases/:usecaseId', {
            template: '<use-case-milestone></use-case-milestone>'
        }).otherwise('/usecases');
    }
]);

angular.module('myMeanApp')
    .constant("BASEURL", "https://mmt-back.herokuapp.com");
