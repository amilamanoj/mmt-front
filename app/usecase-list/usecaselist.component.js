'use strict';

// Register `useCaseList` component, along with its associated controller and template
angular.module('useCase-list').component('useCaseList', {
    templateUrl: 'usecase-list/usecaselist.template.html',
    // controller: ['UseCase', '$sce', (UseCase, $sce) =>
    //     new UseCaseListController(UseCase, $sce) ]
    controller: ['UseCase', '$scope', '$sce', UseCaseListController]
});


function UseCaseListController(UseCase, $scope, $sce) {
    this.usecases = UseCase.query();
    console.log(this.usecases);
    $scope.renderHtml = function (htmlCode) {
        // unsafe
        return $sce.trustAsHtml(htmlCode);
    };
}

