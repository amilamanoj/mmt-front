// define the UseCase service for backend communication
// with np-resource using the factory() method. also using ES6 arrow function
angular.module('myMeanApp')
    .factory('UseCase', ($resource, BASEURL) => $resource(BASEURL + '/api/usecases/:usecaseId', {usecaseId: '@_id'}));
