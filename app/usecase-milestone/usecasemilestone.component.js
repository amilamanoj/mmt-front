'use strict';

// Register `useCaseMilestone` component, along with its associated controller and template
angular.module('useCase-milestone').component('useCaseMilestone', {
    templateUrl: 'usecase-milestone/usecasemilestone.template.html',
    controller: ['UseCase', '$scope', '$routeParams', UseCaseMilestoneController]
});


function UseCaseMilestoneController(UseCase, $scope, $routeParams) {
    var useCase = UseCase.get({usecaseId: $routeParams.usecaseId});
    useCase.$promise.then(function() {
        $scope.ucname = useCase.title;
        // container, items, options
        var container = document.getElementById('visualization');

        var msArray = [];
        for (var milestone of useCase.milestones) {
            console.log(milestone);
            var msVis = {id: milestone.id, content: milestone.name, start: milestone.start_date, end: milestone.end_date}
            msArray.push(msVis);
        }

        console.log(msArray);
        // Create a DataSet (allows two way data-binding)
        var items = new vis.DataSet(msArray);

        // Configuration for the Timeline
        var options = {};

        // Create a Timeline
        var timeline = new vis.Timeline(container, items, options);
    });

}

