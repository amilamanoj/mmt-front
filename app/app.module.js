'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myMeanApp', [
    'ngAnimate',
    'ngRoute',
    'useCase-list',
    'useCase-add',
    'useCase-milestone',
    'ngMaterial',
    'templates',
    'ngMessages'
]);
