# motius-mean-test-frontend application
Motius MEAN stack test project.

## Prerequisites


* nodejs [official website](https://nodejs.org/en/) - nodejs includes [npm](https://www.npmjs.com/) (node package manager)
* bower [official website](http://bower.io/) - frontend dependencies package manager
* gulp [official website](http://gulpjs.com/) - javasript task runner


## Setup (before first run)

* install npm and bower dependencies `npm install`

## Running

* go to the project folder `cd to/your/folder`
* execute gulp tasks (and keep watching for further changes) `gulp watch`
* optional: start built in web server `node node_modules/.bin/http-server` (you can also run the project using your own installation of e.g. XAMPP on windows or even locally). The root folder will be public/

## Directory structure

and important files

```
app/                //test app
-- usecase-add/     // module for adding usecases
-- usecase-list/    // module for listing usecases
-- usecase-milestone// module for drawing usecase milestones
bower_components/   // bower components
node_modules/       // npm modules
public/             // this is the root of (public) website. Do not put stuff there that is not intended for the client
-- index.html       // entry point of the application. **There's only one html page in the application**
-- js/              // js files, 
---- app.js         //  minified and sourcemapped angular app. Created from the files in the app/ directory by gulp
---- templates.js   // angular templates. Created by gulp
-- cs/              // css files.
-- libs/            // third party libs
----libs.js         // all javascript libs (eventually minified)
gulpfile.js         // gulp task specifications
package.json        // npm dependencies information (this belongs into source control)
bower.json          // bower dependencies information (this belongs into source control)
```