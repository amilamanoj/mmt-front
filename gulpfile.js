var gulp = require('gulp');
var mainBowerFiles = require('main-bower-files');
var templateCache = require('gulp-angular-templatecache');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var filter = require('gulp-filter');
var merge = require('merge-stream');
var plumber = require('gulp-plumber');
var babel = require('gulp-babel');
var ngAnnotate = require('gulp-ng-annotate');


var s_filter_i, js_filter, js_filter_i;

(function(){

    function createFilter(file_ending, inverse) {
        if (typeof(file_ending) == "string") { file_ending = [file_ending]}
        var pattern = [];
        if (inverse) {
            pattern.push("**");
        }
        file_ending.forEach(function(e) { pattern.push((inverse?"!":"") + "**/*." + e); });
        return filter(pattern);
    }

    s_filter_i = createFilter('css',true);
    js_filter = createFilter('js');
    js_filter_i = createFilter('js',true)

})();


gulp.task('css', function () {
    return gulp.src([
        //usually only screen.scss
        'bower_components/angular-material/angular-material.css',
        'bower_components/vis/dist/vis.css',
        // no partials
        "!app/**/_*.css"])
        .pipe(plumber())
        .pipe(gulp.dest('public/css'));
});


gulp.task('frontend-libs-copy', function() {

    var all_libs = gulp.src(mainBowerFiles(), { base: 'bower_components' })
        .pipe(plumber())
        //css and scss should be included
        .pipe(s_filter_i);

    var other_libs = all_libs
        .pipe(js_filter_i)
        .pipe(gulp.dest('./public/libs'));

    /**
     * suspend minification due to chrome bug
     * https://bugs.chromium.org/p/chromium/issues/detail?id=327092
     */
    var js_libs = all_libs
        .pipe(js_filter)
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('./public/libs'));

    return merge(other_libs, js_libs);
});

gulp.task('app-js', function () {
    //first list files that define new modules. the module definitions must be at the beginning
    return gulp.src(['app/app.module.js', 'app/**/*.js'])
        .pipe(plumber())
        .pipe(babel())
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(gulp.dest('./public/js'))
});

gulp.task('app-templates', function () {
    return gulp.src('app/**/*.html')
        .pipe(plumber())
        .pipe(templateCache('templates.js', {standalone: true}))
        .pipe(gulp.dest('./public/js'));
});

var MAIN_TASKS = ['app-js', 'app-templates', 'frontend-libs-copy', 'css'];

gulp.task('watch', MAIN_TASKS, function () {
    gulp.watch('app/ng/**/*.js', ['app-js']);
    gulp.watch('app/ng/**/*.html', [ 'app-templates']);
    gulp.watch('bower.json', [ 'frontend-libs-copy']);
});

gulp.task('install', MAIN_TASKS);

gulp.task('clean', function () {
    return gulp.src([
                        'public/js/app.js',
                        'public/js/templates.js',
                        'public/css',
                        'public/libs'], {read: false})
        .pipe(clean());
});





